#! /usr/bin/env python

import os
from os.path import abspath, isfile, join


def lcg():
    """
    Generates the code to set up an LCG environment.
    """

    # The CMAKE_PREFIX_PATH seems to point to the root of the environment.
    location = os.environ.get('CMAKE_PREFIX_PATH')
    setup = join(location, 'setup.sh')
    if not isfile(setup):
        raise ValueError('Could not find LCG setup.sh')

    return 'source ' + setup


def conda_like():
    r"""
    Generates the code to set up a conda/mamba/micromamba environment.

    \note Only micromamba is officially supported.
    """

    env_name = os.environ['CONDA_DEFAULT_ENV']

    # Figure out which variant we're using
    if 'MAMBA_EXE' in os.environ and 'MAMBA_ROOT_PREFIX' in os.environ:
        ## \todo Is this also valid for environments created by mamba?
        template = '''
# Make sure {mamba_exe_name} is set up
export MAMBA_EXE='{mamba_exe}';
export MAMBA_ROOT_PREFIX='{mamba_root}';
__mamba_setup="$("$MAMBA_EXE" shell hook --shell bash --root-prefix "$MAMBA_ROOT_PREFIX" 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__mamba_setup"
else
    alias {mamba_exe_name}="$MAMBA_EXE"  # Fallback on help from mamba activate
fi
unset __mamba_setup

# Enable the environment
{mamba_exe_name} activate '{env_name}'
'''
        mamba_exe = os.environ['MAMBA_EXE']
        mamba_root = os.environ['MAMBA_ROOT_PREFIX']
        mamba_exe_name = 'micromamba' if mamba_exe.endswith('micromamba') else 'mamba'
        return template.format(mamba_exe=mamba_exe,
                               mamba_root=mamba_root,
                               mamba_exe_name=mamba_exe_name,
                               env_name=env_name)
    elif 'CONDA_EXE' in os.environ:
        template = '''
# Make sure conda is set up
__conda_setup="$('{conda_exe}' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "{conda_base}/etc/profile.d/conda.sh" ]; then
        . "{conda_base}/etc/profile.d/conda.sh"
    else
        export PATH="{conda_base}/bin:$PATH"
    fi
fi
unset __conda_setup

# Enable the environment
conda activate '{env_name}'
'''
        conda_exe = os.environ['CONDA_EXE']
        conda_root = abspath(join(conda_exe, os.pardir, os.pardir))
        return template.format(conda_exe=conda_exe,
                               conda_root=conda_root,
                               env_name=env_name)
    else:
        raise ValueError('Unknown conda-like environment')


def detect_environment():
    """
    Detect the environment in which the command is begin run, and returns code
    to set it up again. Returns an empty string on failure.
    """

    # List of environment variables used to detect which environment is active.
    detect = {
        'LCG_VERSION': lcg,
        'CONDA_DEFAULT_ENV': conda_like,
    }

    for env_var, function in detect.items():
        if env_var in os.environ:
            try:
                return function()
            except:
                pass  # Try other possibilities

    return ''


if __name__ == '__main__':
    print(detect_environment())
