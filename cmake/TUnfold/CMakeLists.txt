# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

cmake_minimum_required(VERSION 3.23..3.26 FATAL_ERROR)
project(TUnfold VERSION 17.9 LANGUAGES CXX)

find_package(ROOT 6.24 REQUIRED)

set(classes
    TUnfoldBinning
    TUnfoldBinningXML
    TUnfoldDensity
    TUnfoldIterativeEM
    TUnfoldSys
    TUnfold)

# Library
set(sources ${classes})
list(TRANSFORM sources APPEND V${TUnfold_VERSION_MAJOR}.cxx)

set(headers ${classes})
list(TRANSFORM headers APPEND .h)

add_library(TUnfold SHARED ${sources})
target_sources(TUnfold PUBLIC FILE_SET HEADERS FILES ${headers})
target_link_libraries(TUnfold PUBLIC ROOT::Hist ROOT::XMLParser)

# Install
include(GNUInstallDirs)
set(include_dir "${CMAKE_INSTALL_INCLUDEDIR}/TUnfold")
install(TARGETS TUnfold
        EXPORT TUnfoldTargets
        COMPONENT TUnfold
        FILE_SET HEADERS DESTINATION "${include_dir}"
        RUNTIME  DESTINATION "${CMAKE_INSTALL_LIBDIR}"
        INCLUDES DESTINATION "${include_dir}")

# Dictionary
configure_file(LinkDef.h.in LinkDef.h)
# https://github.com/root-project/root/issues/8308#issuecomment-1143791946
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
root_generate_dictionary(TUnfoldV${TUnfold_VERSION_MAJOR}Dict
                         ${headers}
                         MODULE TUnfold
                         LINKDEF "${CMAKE_CURRENT_BINARY_DIR}/LinkDef.h")

# Export
include(CMakePackageConfigHelpers)
set(CMAKECONFIG_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/TUnfold")
configure_package_config_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/TUnfoldConfig.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/TUnfoldConfig.cmake"
    INSTALL_DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
)
write_basic_package_version_file(
    TUnfoldConfigVersion.cmake
    COMPATIBILITY SameMajorVersion
)
install(FILES
        "${CMAKE_CURRENT_BINARY_DIR}/TUnfoldConfig.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/TUnfoldConfigVersion.cmake"
        DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
        COMPONENT Devel)
install(EXPORT TUnfoldTargets
        NAMESPACE TUnfold::
        DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
        FILE TUnfoldTargets.cmake)
