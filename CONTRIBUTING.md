# Contributing to `Core`

## Applying local changes

If you modify part of the source code of a module (e.g. `Core`), you can recompile it as follows:
```sh
cmake --build $DAS_BASE/build/Core --target install
```
This is analog to `(cd $CMSSW_BASE; scram b)`.
It is good practice to check that your changes do not harm the rest of the code. For this, tests can be run as follows:
```sh
ctest --test-dir $DAS_BASE/build/Core
```
which is analog to `(cd $CMSSW_BASE; scram b runtests)`.

For `cmake` and `ctest`, one can always specify `-j8` to compile with 8 cores (for instance), or more generally `-j$(nproc)` to compile with the number of cores on the present machine.

Despite multitasking, the whole process of compiling and testing may take a few minutes. In case you would like to compile and/or test just one executable or library, do the following (where the `JEC` library is here just taken as an example):
```sh
cmake --build $DAS_BASE/build/Core --target JEC
```
which is analog to `(cd $CMSSW_BASE/src/Core/JEC; scram b)`. Commands will differ if you specified a generator other than GNU Makefiles.

Whenever developing, it is perfectly fine to only compile subsets of the code to speed up the whole process. Whenever running over large samples, it is instead advised to commit and to rerun the whole `Core` to ensure compatibility and reproducibility of the results.

Also, for convenience, an alias has been defined: `quick Core` to compile `Core` or `quick Core/JEC` to compile `Core/JEC`.

Finally, if you apply any changes in the n-tupliser or the `Objects` module, you can only test it via a container in CMSSW (which may not be possible on all machines):
```sh
cd $DAS_BASE/CMSSW_12_4_0
das-cmssw el8 scram b runtests
```
(Remember that scram returns 0 even if the tests have failed. Read through the output carefully to know if the code has worked.)

## Updating a module

If you want to update the source code of a module (e.g. `Core`), assuming no local changes, you can proceed as follows:
```sh
cd $DAS_BASE/Core
git pull
cmake -B $DAS_BASE/build
cmake --build $DAS_BASE/build --target install
```
In case of local changes, you should first commit them, or use `git stash` to pause the changes temporarily (after the update, it will be sufficient to run `git stash pop` to recover your changes).

## Add a module

### Method \#1: The automated way with `template` (only for executables)

Use `template` to add a basic file in a new or existing module. This automatically updates the `CMakeLists.txt` files with the necessary info. You may then edit the file and compile it as explained above.

### Method \#2: By hand, step by step

Proceed as follows to add a module in `Core`:
1. add a new `add_subdirectory()` statement in `Core/CMakeLists.txt`;
2. create a corresponding subdirectory in `Core`, where the code will be hosted;
3. the structure of a modules follows [similar principles as in CMSSW](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideBuildFile#CmsswSrcCodeDir):
    - the C++ code of executables should be stored in a further subdirectory called `bin`,
    - the C++ code of headers in `interface`,
    - the C++ code of the library in `src`,
    - the tests in `test`,
    - shell and Python scripts should go in `scripts`,
    - Python libraries should be stored in `python`;
4. add a new `CMakeLists.txt` in the new subdirectory with relevant `core_add_library()` and `core_add_executable()`:
```cmake
core_add_library(                    # this block only in case of files in `src` or `test`
    SOURCES file1.cc [file2.cc ...]  # source files in `src` (at least one is required)
    TESTS JMEmatching                # name of the file in `test` with the same name (although truncated from the `.cc` extension)
)
core_add_executable(                 # repeat this block for every executable in `bin`, must come after `core_add_library`
    applyJERsmearing                 # name of the file in `bin` with the same name (although truncated from the `.cc` extension)
    LIBRARIES correctionlib          # name of dependencies (other Core modules or external libraries); this line is optional
)
```
The definition of `core_add_library()` and `core_add_executable()` can be found in `$DAS_BASE/Core/cmake/CoreHelpers.cmake`.

## Making a new merge request

- In the description of the merge request, describe the issuea or point to an existing issue, and describe the solution that you propose with the present merge request.
- Use the labels:
   - for the changes (patch by default; minor if n-tuples may need to be reproduced; major if the whole software needs to be reinstalled);
   - for the status (a priori ongoing, but you may change when appropriate);
   - for the type (software vs physics);
   - miscellaneous (e.g. urgent).
- Open points to be discussed with the reviewer should be described in subsequent threads in the discussion of the merge request.
   - Press "resolve" only if there is no doubt, otherwise let the reviewer do it.

A good trick is to learn how to make interactive rebases (`git rebase -i`) so that the commits of your MR tell a meaningful story.

## Good practices

Fight against the increase of entropy by
- pulling regularly,
- committing regularly,
- documenting your code,
- implementing tests for your developments!

Happy analysis!
