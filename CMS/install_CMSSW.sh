#!/usr/bin/env bash

source /cvmfs/cms.cern.ch/cmsset_default.sh

set -e

if [[ $# -ne 2 ]]; then
    echo "$0 CMSSW_X_Y_Z JetToolbox_branch" >&2
    exit 2
fi

CMSSW_VERSION="$1"
JETTOOLBOX_BRANCH="$2"

# Get CMSSW if it doesn't exist
[ ! -d $CMSSW_VERSION ] && scram p $CMSSW_VERSION

# Silence perl warnings (from scram)
export LANG=C
export LC_ALL=C

# Build!
cd $CMSSW_VERSION/src
eval $(scram runtime -sh) # cmsenv

# And JetToolbox
[ ! -d JMEAnalysis/JetToolbox ] && (
    git clone https://github.com/cms-jet/JetToolbox.git JMEAnalysis/JetToolbox -b "$JETTOOLBOX_BRANCH"

    if [ -f $DAS_BASE/CMS/${CMSSW_VERSION}_${JETTOOLBOX_BRANCH}.patch ]; then
        cd JMEAnalysis/JetToolbox
        patch -p1 <$DAS_BASE/CMS/${CMSSW_VERSION}_${JETTOOLBOX_BRANCH}.patch
    else
        echo "\x1B[33mThe jet toolbox may need to be patched.\x1B[0m"
    fi
)

# Set up symlinks to the relevant parts of Core
mkdir -p $CMSSW_BASE/src/Core
cd $CMSSW_BASE/src/Core
[ ! -d Objects   ] && ln -s $DAS_BASE/Core/Objects
[ ! -d Ntupliser ] && ln -s $DAS_BASE/Core/Ntupliser
echo $CMSSW_BASE/src/Core/Ntupliser/plugins/UserInfo.h
echo $DAS_BASE/Darwin/interface/UserInfo.h

# Set up symlinks to the relevant parts of Darwin
cd Ntupliser/plugins
[ ! -f UserInfo.h  ] && ln -s $DAS_BASE/Darwin/interface/UserInfo.h
[ ! -f UserInfo.cc ] && ln -s $DAS_BASE/Darwin/src/UserInfo.cc
[ ! -f colours.h   ] && ln -s $DAS_BASE/Darwin/interface/colours.h

# Compile
cd $CMSSW_BASE
scram b -j$(nproc)
scram setup $DAS_BASE/build/CMS/das.xml
